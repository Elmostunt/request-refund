package com.latam.proref.seat.entities;

import lombok.Data;

@Data
public class AirportInfo {

	String iataCode;
}
