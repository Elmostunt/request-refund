package com.latam.proref.seat.entities;

import lombok.Data;

@Data
public class DepartureInfo {

	AirportInfo airportInfo;
	City city;
}
