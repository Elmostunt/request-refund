package com.latam.proref.seat.entities;

import lombok.Data;

@Data
public class SeatRQ {

	Flight flight;
	Pnr pnr;
}
