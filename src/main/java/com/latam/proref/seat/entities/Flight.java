package com.latam.proref.seat.entities;

import lombok.Data;

@Data
public class Flight {
	
	Integer flightNumber;
    String cabinClass;
    String airlineCode;
    String flightDate;
    DepartureInfo departureAirport;
    ArrivalAirport arrivalAirport;
}
