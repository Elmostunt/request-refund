package com.latam.proref.seat.entities;

import lombok.Data;

@Data
public class City {
	String isoCode;
}
