package com.latam.proref.requestrefund.entities;

import lombok.Data;

@Data
public class RequestRefundRQ {

	Integer flightNumber;
	String airlineCode;
	String flightDate;
}
