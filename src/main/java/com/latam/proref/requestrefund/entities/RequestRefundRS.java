package com.latam.proref.requestrefund.entities;

import lombok.Data;

@Data
public class RequestRefundRS {
	String name;
	String status;
	String dateCreation;
}
