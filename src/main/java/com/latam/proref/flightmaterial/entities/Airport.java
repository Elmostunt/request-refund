package com.latam.proref.flightmaterial.entities;

import lombok.Data;

@Data
public class Airport {

	AirportInfo airportInfo;
	City city;
}
