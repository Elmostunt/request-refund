package com.latam.proref.flightmaterial.entities;

import lombok.Data;

@Data
public class Flight {

	Integer flightNumber;
	String cabinClass;
	String airlineCode;
	//TODO Validar formato de fecha ($ddMMM)
	String flightDate;
	Airport departureAirport;
	Airport arrivalAirport;
	
}
