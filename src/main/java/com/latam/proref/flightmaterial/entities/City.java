package com.latam.proref.flightmaterial.entities;

import lombok.Data;

@Data
public class City {
	String isoCode;
}
