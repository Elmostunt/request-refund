package com.latam.proref.flightmaterial.entities;

import lombok.Data;

@Data
public class FlightMaterialConfigRS {

	String materialCode;
	String configNumber; 
}
