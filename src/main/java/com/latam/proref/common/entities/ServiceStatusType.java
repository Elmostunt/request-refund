package com.latam.proref.common.entities;

import lombok.Data;

@Data
public class ServiceStatusType {

	String message;
	String nativeMessage;
}
