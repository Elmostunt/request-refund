//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.01.17 a las 01:17:46 AM CLT 
//


package com.latam.proref.mdb.events;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.latam.proref.mdb.entities.AircraftType;
import com.latam.proref.mdb.entities.LegType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Flight">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="airlineCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="flightNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="flightDateUTC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Leg">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://entities.latam.com/v3_0}LegType">
 *                 &lt;sequence>
 *                   &lt;element name="Aircraft" type="{http://entities.latam.com/v3_0}AircraftType"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="delay" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "flight",
    "leg",
    "delay"
})
@XmlRootElement(name = "FlightLegTakeOffEVT")
public class FlightLegTakeOffEVT {

    @XmlElement(name = "Flight", required = true)
    protected FlightLegTakeOffEVT.Flight flight;
    @XmlElement(name = "Leg", required = true)
    protected FlightLegTakeOffEVT.Leg leg;
    protected int delay;

    /**
     * Obtiene el valor de la propiedad flight.
     * 
     * @return
     *     possible object is
     *     {@link FlightLegTakeOffEVT.Flight }
     *     
     */
    public FlightLegTakeOffEVT.Flight getFlight() {
        return flight;
    }

    /**
     * Define el valor de la propiedad flight.
     * 
     * @param value
     *     allowed object is
     *     {@link FlightLegTakeOffEVT.Flight }
     *     
     */
    public void setFlight(FlightLegTakeOffEVT.Flight value) {
        this.flight = value;
    }

    /**
     * Obtiene el valor de la propiedad leg.
     * 
     * @return
     *     possible object is
     *     {@link FlightLegTakeOffEVT.Leg }
     *     
     */
    public FlightLegTakeOffEVT.Leg getLeg() {
        return leg;
    }

    /**
     * Define el valor de la propiedad leg.
     * 
     * @param value
     *     allowed object is
     *     {@link FlightLegTakeOffEVT.Leg }
     *     
     */
    public void setLeg(FlightLegTakeOffEVT.Leg value) {
        this.leg = value;
    }

    /**
     * Obtiene el valor de la propiedad delay.
     * 
     */
    public int getDelay() {
        return delay;
    }

    /**
     * Define el valor de la propiedad delay.
     * 
     */
    public void setDelay(int value) {
        this.delay = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="airlineCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="flightNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="flightDateUTC" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "airlineCode",
        "flightNumber",
        "flightDateUTC"
    })
    public static class Flight {

        @XmlElement(required = true)
        protected String airlineCode;
        @XmlElement(required = true)
        protected String flightNumber;
        @XmlElement(required = true)
        protected String flightDateUTC;

        /**
         * Obtiene el valor de la propiedad airlineCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAirlineCode() {
            return airlineCode;
        }

        /**
         * Define el valor de la propiedad airlineCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAirlineCode(String value) {
            this.airlineCode = value;
        }

        /**
         * Obtiene el valor de la propiedad flightNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFlightNumber() {
            return flightNumber;
        }

        /**
         * Define el valor de la propiedad flightNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFlightNumber(String value) {
            this.flightNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad flightDateUTC.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFlightDateUTC() {
            return flightDateUTC;
        }

        /**
         * Define el valor de la propiedad flightDateUTC.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFlightDateUTC(String value) {
            this.flightDateUTC = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://entities.latam.com/v3_0}LegType">
     *       &lt;sequence>
     *         &lt;element name="Aircraft" type="{http://entities.latam.com/v3_0}AircraftType"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "aircraft"
    })
    public static class Leg
        extends LegType
    {

        @XmlElement(name = "Aircraft", required = true)
        protected AircraftType aircraft;

        /**
         * Obtiene el valor de la propiedad aircraft.
         * 
         * @return
         *     possible object is
         *     {@link AircraftType }
         *     
         */
        public AircraftType getAircraft() {
            return aircraft;
        }

        /**
         * Define el valor de la propiedad aircraft.
         * 
         * @param value
         *     allowed object is
         *     {@link AircraftType }
         *     
         */
        public void setAircraft(AircraftType value) {
            this.aircraft = value;
        }

    }

}
