//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.01.17 a las 01:17:46 AM CLT 
//


package com.latam.proref.mdb.entities;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.latam.entities.v3_0 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.latam.entities.v3_0
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LegType }
     * 
     */
    public LegType createLegType() {
        return new LegType();
    }

    /**
     * Create an instance of {@link LegType.Arrival }
     * 
     */
    public LegType.Arrival createLegTypeArrival() {
        return new LegType.Arrival();
    }

    /**
     * Create an instance of {@link LegType.Arrival.BlockOn }
     * 
     */
    public LegType.Arrival.BlockOn createLegTypeArrivalBlockOn() {
        return new LegType.Arrival.BlockOn();
    }

    /**
     * Create an instance of {@link LegType.Arrival.TouchDown }
     * 
     */
    public LegType.Arrival.TouchDown createLegTypeArrivalTouchDown() {
        return new LegType.Arrival.TouchDown();
    }

    /**
     * Create an instance of {@link LegType.Departure }
     * 
     */
    public LegType.Departure createLegTypeDeparture() {
        return new LegType.Departure();
    }

    /**
     * Create an instance of {@link LegType.Departure.TakeOff }
     * 
     */
    public LegType.Departure.TakeOff createLegTypeDepartureTakeOff() {
        return new LegType.Departure.TakeOff();
    }

    /**
     * Create an instance of {@link LegType.Departure.BlockOff }
     * 
     */
    public LegType.Departure.BlockOff createLegTypeDepartureBlockOff() {
        return new LegType.Departure.BlockOff();
    }

    /**
     * Create an instance of {@link AircraftType }
     * 
     */
    public AircraftType createAircraftType() {
        return new AircraftType();
    }

    /**
     * Create an instance of {@link LegType.AirlineCodes }
     * 
     */
    public LegType.AirlineCodes createLegTypeAirlineCodes() {
        return new LegType.AirlineCodes();
    }

    /**
     * Create an instance of {@link LegType.OperationalInformation }
     * 
     */
    public LegType.OperationalInformation createLegTypeOperationalInformation() {
        return new LegType.OperationalInformation();
    }

    /**
     * Create an instance of {@link LegType.FlightServiceType }
     * 
     */
    public LegType.FlightServiceType createLegTypeFlightServiceType() {
        return new LegType.FlightServiceType();
    }

    /**
     * Create an instance of {@link LegType.Arrival.AirportIATACode }
     * 
     */
    public LegType.Arrival.AirportIATACode createLegTypeArrivalAirportIATACode() {
        return new LegType.Arrival.AirportIATACode();
    }

    /**
     * Create an instance of {@link LegType.Arrival.BlockOn.ScheduleDateTime }
     * 
     */
    public LegType.Arrival.BlockOn.ScheduleDateTime createLegTypeArrivalBlockOnScheduleDateTime() {
        return new LegType.Arrival.BlockOn.ScheduleDateTime();
    }

    /**
     * Create an instance of {@link LegType.Arrival.BlockOn.RealDateTime }
     * 
     */
    public LegType.Arrival.BlockOn.RealDateTime createLegTypeArrivalBlockOnRealDateTime() {
        return new LegType.Arrival.BlockOn.RealDateTime();
    }

    /**
     * Create an instance of {@link LegType.Arrival.BlockOn.EstimatedDateTime }
     * 
     */
    public LegType.Arrival.BlockOn.EstimatedDateTime createLegTypeArrivalBlockOnEstimatedDateTime() {
        return new LegType.Arrival.BlockOn.EstimatedDateTime();
    }

    /**
     * Create an instance of {@link LegType.Arrival.TouchDown.RealDateTime }
     * 
     */
    public LegType.Arrival.TouchDown.RealDateTime createLegTypeArrivalTouchDownRealDateTime() {
        return new LegType.Arrival.TouchDown.RealDateTime();
    }

    /**
     * Create an instance of {@link LegType.Arrival.TouchDown.EstimatedDateTime }
     * 
     */
    public LegType.Arrival.TouchDown.EstimatedDateTime createLegTypeArrivalTouchDownEstimatedDateTime() {
        return new LegType.Arrival.TouchDown.EstimatedDateTime();
    }

    /**
     * Create an instance of {@link LegType.Departure.AirportIATACode }
     * 
     */
    public LegType.Departure.AirportIATACode createLegTypeDepartureAirportIATACode() {
        return new LegType.Departure.AirportIATACode();
    }

    /**
     * Create an instance of {@link LegType.Departure.TakeOff.RealDateTime }
     * 
     */
    public LegType.Departure.TakeOff.RealDateTime createLegTypeDepartureTakeOffRealDateTime() {
        return new LegType.Departure.TakeOff.RealDateTime();
    }

    /**
     * Create an instance of {@link LegType.Departure.TakeOff.EstimatedDateTime }
     * 
     */
    public LegType.Departure.TakeOff.EstimatedDateTime createLegTypeDepartureTakeOffEstimatedDateTime() {
        return new LegType.Departure.TakeOff.EstimatedDateTime();
    }

    /**
     * Create an instance of {@link LegType.Departure.BlockOff.ScheduleDateTime }
     * 
     */
    public LegType.Departure.BlockOff.ScheduleDateTime createLegTypeDepartureBlockOffScheduleDateTime() {
        return new LegType.Departure.BlockOff.ScheduleDateTime();
    }

    /**
     * Create an instance of {@link LegType.Departure.BlockOff.RealDateTime }
     * 
     */
    public LegType.Departure.BlockOff.RealDateTime createLegTypeDepartureBlockOffRealDateTime() {
        return new LegType.Departure.BlockOff.RealDateTime();
    }

    /**
     * Create an instance of {@link LegType.Departure.BlockOff.EstimatedDateTime }
     * 
     */
    public LegType.Departure.BlockOff.EstimatedDateTime createLegTypeDepartureBlockOffEstimatedDateTime() {
        return new LegType.Departure.BlockOff.EstimatedDateTime();
    }

}
