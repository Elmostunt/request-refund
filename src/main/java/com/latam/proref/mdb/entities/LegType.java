//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.01.17 a las 01:17:46 AM CLT 
//


package com.latam.proref.mdb.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * Entidad de negocio que representa informaci�n operacional de los leg o tramos.
 * 
 * <p>Clase Java para LegType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="LegType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AirlineCodes">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="operating" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="comercial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="owner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Departure">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="airportIATACode">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="BlockOff">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ScheduleDateTime">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="RealDateTime" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="EstimatedDateTime" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="TakeOff" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="RealDateTime" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="EstimatedDateTime" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Arrival">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="airportIATACode">
 *                     &lt;complexType>
 *                       &lt;simpleContent>
 *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                         &lt;/extension>
 *                       &lt;/simpleContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="TouchDown" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="RealDateTime" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="EstimatedDateTime" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="BlockOn">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ScheduleDateTime">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="RealDateTime" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="EstimatedDateTime" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OperationalInformation" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="legStatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="itineraryStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="operationalStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="suffixCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="FlightServiceType" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="routeTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LegType", propOrder = {
    "airlineCodes",
    "departure",
    "arrival",
    "operationalInformation",
    "flightServiceType"
})
@XmlSeeAlso({
    com.latam.proref.mdb.events.FlightLegTakeOffEVT.Leg.class
})
public class LegType {

    @XmlElement(name = "AirlineCodes", required = true)
    protected LegType.AirlineCodes airlineCodes;
    @XmlElement(name = "Departure", required = true)
    protected LegType.Departure departure;
    @XmlElement(name = "Arrival", required = true)
    protected LegType.Arrival arrival;
    @XmlElement(name = "OperationalInformation")
    protected LegType.OperationalInformation operationalInformation;
    @XmlElement(name = "FlightServiceType")
    protected LegType.FlightServiceType flightServiceType;

    /**
     * Obtiene el valor de la propiedad airlineCodes.
     * 
     * @return
     *     possible object is
     *     {@link LegType.AirlineCodes }
     *     
     */
    public LegType.AirlineCodes getAirlineCodes() {
        return airlineCodes;
    }

    /**
     * Define el valor de la propiedad airlineCodes.
     * 
     * @param value
     *     allowed object is
     *     {@link LegType.AirlineCodes }
     *     
     */
    public void setAirlineCodes(LegType.AirlineCodes value) {
        this.airlineCodes = value;
    }

    /**
     * Obtiene el valor de la propiedad departure.
     * 
     * @return
     *     possible object is
     *     {@link LegType.Departure }
     *     
     */
    public LegType.Departure getDeparture() {
        return departure;
    }

    /**
     * Define el valor de la propiedad departure.
     * 
     * @param value
     *     allowed object is
     *     {@link LegType.Departure }
     *     
     */
    public void setDeparture(LegType.Departure value) {
        this.departure = value;
    }

    /**
     * Obtiene el valor de la propiedad arrival.
     * 
     * @return
     *     possible object is
     *     {@link LegType.Arrival }
     *     
     */
    public LegType.Arrival getArrival() {
        return arrival;
    }

    /**
     * Define el valor de la propiedad arrival.
     * 
     * @param value
     *     allowed object is
     *     {@link LegType.Arrival }
     *     
     */
    public void setArrival(LegType.Arrival value) {
        this.arrival = value;
    }

    /**
     * Obtiene el valor de la propiedad operationalInformation.
     * 
     * @return
     *     possible object is
     *     {@link LegType.OperationalInformation }
     *     
     */
    public LegType.OperationalInformation getOperationalInformation() {
        return operationalInformation;
    }

    /**
     * Define el valor de la propiedad operationalInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link LegType.OperationalInformation }
     *     
     */
    public void setOperationalInformation(LegType.OperationalInformation value) {
        this.operationalInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad flightServiceType.
     * 
     * @return
     *     possible object is
     *     {@link LegType.FlightServiceType }
     *     
     */
    public LegType.FlightServiceType getFlightServiceType() {
        return flightServiceType;
    }

    /**
     * Define el valor de la propiedad flightServiceType.
     * 
     * @param value
     *     allowed object is
     *     {@link LegType.FlightServiceType }
     *     
     */
    public void setFlightServiceType(LegType.FlightServiceType value) {
        this.flightServiceType = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="operating" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="comercial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="owner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "operating",
        "comercial",
        "owner"
    })
    public static class AirlineCodes {

        @XmlElement(required = true)
        protected String operating;
        protected String comercial;
        protected String owner;

        /**
         * Obtiene el valor de la propiedad operating.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOperating() {
            return operating;
        }

        /**
         * Define el valor de la propiedad operating.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOperating(String value) {
            this.operating = value;
        }

        /**
         * Obtiene el valor de la propiedad comercial.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getComercial() {
            return comercial;
        }

        /**
         * Define el valor de la propiedad comercial.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setComercial(String value) {
            this.comercial = value;
        }

        /**
         * Obtiene el valor de la propiedad owner.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOwner() {
            return owner;
        }

        /**
         * Define el valor de la propiedad owner.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOwner(String value) {
            this.owner = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="airportIATACode">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="TouchDown" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="RealDateTime" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="EstimatedDateTime" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="BlockOn">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ScheduleDateTime">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="RealDateTime" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="EstimatedDateTime" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "airportIATACode",
        "touchDown",
        "blockOn"
    })
    public static class Arrival {

        @XmlElement(required = true)
        protected LegType.Arrival.AirportIATACode airportIATACode;
        @XmlElement(name = "TouchDown")
        protected LegType.Arrival.TouchDown touchDown;
        @XmlElement(name = "BlockOn", required = true)
        protected LegType.Arrival.BlockOn blockOn;

        /**
         * Obtiene el valor de la propiedad airportIATACode.
         * 
         * @return
         *     possible object is
         *     {@link LegType.Arrival.AirportIATACode }
         *     
         */
        public LegType.Arrival.AirportIATACode getAirportIATACode() {
            return airportIATACode;
        }

        /**
         * Define el valor de la propiedad airportIATACode.
         * 
         * @param value
         *     allowed object is
         *     {@link LegType.Arrival.AirportIATACode }
         *     
         */
        public void setAirportIATACode(LegType.Arrival.AirportIATACode value) {
            this.airportIATACode = value;
        }

        /**
         * Obtiene el valor de la propiedad touchDown.
         * 
         * @return
         *     possible object is
         *     {@link LegType.Arrival.TouchDown }
         *     
         */
        public LegType.Arrival.TouchDown getTouchDown() {
            return touchDown;
        }

        /**
         * Define el valor de la propiedad touchDown.
         * 
         * @param value
         *     allowed object is
         *     {@link LegType.Arrival.TouchDown }
         *     
         */
        public void setTouchDown(LegType.Arrival.TouchDown value) {
            this.touchDown = value;
        }

        /**
         * Obtiene el valor de la propiedad blockOn.
         * 
         * @return
         *     possible object is
         *     {@link LegType.Arrival.BlockOn }
         *     
         */
        public LegType.Arrival.BlockOn getBlockOn() {
            return blockOn;
        }

        /**
         * Define el valor de la propiedad blockOn.
         * 
         * @param value
         *     allowed object is
         *     {@link LegType.Arrival.BlockOn }
         *     
         */
        public void setBlockOn(LegType.Arrival.BlockOn value) {
            this.blockOn = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class AirportIATACode {

            @XmlValue
            protected String value;

            /**
             * Obtiene el valor de la propiedad value.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Define el valor de la propiedad value.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ScheduleDateTime">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="RealDateTime" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="EstimatedDateTime" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "scheduleDateTime",
            "realDateTime",
            "estimatedDateTime"
        })
        public static class BlockOn {

            @XmlElement(name = "ScheduleDateTime", required = true)
            protected LegType.Arrival.BlockOn.ScheduleDateTime scheduleDateTime;
            @XmlElement(name = "RealDateTime")
            protected LegType.Arrival.BlockOn.RealDateTime realDateTime;
            @XmlElement(name = "EstimatedDateTime")
            protected LegType.Arrival.BlockOn.EstimatedDateTime estimatedDateTime;

            /**
             * Obtiene el valor de la propiedad scheduleDateTime.
             * 
             * @return
             *     possible object is
             *     {@link LegType.Arrival.BlockOn.ScheduleDateTime }
             *     
             */
            public LegType.Arrival.BlockOn.ScheduleDateTime getScheduleDateTime() {
                return scheduleDateTime;
            }

            /**
             * Define el valor de la propiedad scheduleDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link LegType.Arrival.BlockOn.ScheduleDateTime }
             *     
             */
            public void setScheduleDateTime(LegType.Arrival.BlockOn.ScheduleDateTime value) {
                this.scheduleDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad realDateTime.
             * 
             * @return
             *     possible object is
             *     {@link LegType.Arrival.BlockOn.RealDateTime }
             *     
             */
            public LegType.Arrival.BlockOn.RealDateTime getRealDateTime() {
                return realDateTime;
            }

            /**
             * Define el valor de la propiedad realDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link LegType.Arrival.BlockOn.RealDateTime }
             *     
             */
            public void setRealDateTime(LegType.Arrival.BlockOn.RealDateTime value) {
                this.realDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad estimatedDateTime.
             * 
             * @return
             *     possible object is
             *     {@link LegType.Arrival.BlockOn.EstimatedDateTime }
             *     
             */
            public LegType.Arrival.BlockOn.EstimatedDateTime getEstimatedDateTime() {
                return estimatedDateTime;
            }

            /**
             * Define el valor de la propiedad estimatedDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link LegType.Arrival.BlockOn.EstimatedDateTime }
             *     
             */
            public void setEstimatedDateTime(LegType.Arrival.BlockOn.EstimatedDateTime value) {
                this.estimatedDateTime = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "utc",
                "local"
            })
            public static class EstimatedDateTime {

                @XmlElement(required = true)
                protected String utc;
                protected String local;

                /**
                 * Obtiene el valor de la propiedad utc.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUtc() {
                    return utc;
                }

                /**
                 * Define el valor de la propiedad utc.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUtc(String value) {
                    this.utc = value;
                }

                /**
                 * Obtiene el valor de la propiedad local.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocal() {
                    return local;
                }

                /**
                 * Define el valor de la propiedad local.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocal(String value) {
                    this.local = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "utc",
                "local"
            })
            public static class RealDateTime {

                @XmlElement(required = true)
                protected String utc;
                protected String local;

                /**
                 * Obtiene el valor de la propiedad utc.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUtc() {
                    return utc;
                }

                /**
                 * Define el valor de la propiedad utc.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUtc(String value) {
                    this.utc = value;
                }

                /**
                 * Obtiene el valor de la propiedad local.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocal() {
                    return local;
                }

                /**
                 * Define el valor de la propiedad local.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocal(String value) {
                    this.local = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "utc",
                "local"
            })
            public static class ScheduleDateTime {

                @XmlElement(required = true)
                protected String utc;
                protected String local;

                /**
                 * Obtiene el valor de la propiedad utc.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUtc() {
                    return utc;
                }

                /**
                 * Define el valor de la propiedad utc.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUtc(String value) {
                    this.utc = value;
                }

                /**
                 * Obtiene el valor de la propiedad local.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocal() {
                    return local;
                }

                /**
                 * Define el valor de la propiedad local.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocal(String value) {
                    this.local = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="RealDateTime" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="EstimatedDateTime" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "realDateTime",
            "estimatedDateTime"
        })
        public static class TouchDown {

            @XmlElement(name = "RealDateTime")
            protected LegType.Arrival.TouchDown.RealDateTime realDateTime;
            @XmlElement(name = "EstimatedDateTime")
            protected LegType.Arrival.TouchDown.EstimatedDateTime estimatedDateTime;

            /**
             * Obtiene el valor de la propiedad realDateTime.
             * 
             * @return
             *     possible object is
             *     {@link LegType.Arrival.TouchDown.RealDateTime }
             *     
             */
            public LegType.Arrival.TouchDown.RealDateTime getRealDateTime() {
                return realDateTime;
            }

            /**
             * Define el valor de la propiedad realDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link LegType.Arrival.TouchDown.RealDateTime }
             *     
             */
            public void setRealDateTime(LegType.Arrival.TouchDown.RealDateTime value) {
                this.realDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad estimatedDateTime.
             * 
             * @return
             *     possible object is
             *     {@link LegType.Arrival.TouchDown.EstimatedDateTime }
             *     
             */
            public LegType.Arrival.TouchDown.EstimatedDateTime getEstimatedDateTime() {
                return estimatedDateTime;
            }

            /**
             * Define el valor de la propiedad estimatedDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link LegType.Arrival.TouchDown.EstimatedDateTime }
             *     
             */
            public void setEstimatedDateTime(LegType.Arrival.TouchDown.EstimatedDateTime value) {
                this.estimatedDateTime = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "utc",
                "local"
            })
            public static class EstimatedDateTime {

                @XmlElement(required = true)
                protected String utc;
                protected String local;

                /**
                 * Obtiene el valor de la propiedad utc.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUtc() {
                    return utc;
                }

                /**
                 * Define el valor de la propiedad utc.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUtc(String value) {
                    this.utc = value;
                }

                /**
                 * Obtiene el valor de la propiedad local.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocal() {
                    return local;
                }

                /**
                 * Define el valor de la propiedad local.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocal(String value) {
                    this.local = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "utc",
                "local"
            })
            public static class RealDateTime {

                @XmlElement(required = true)
                protected String utc;
                protected String local;

                /**
                 * Obtiene el valor de la propiedad utc.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUtc() {
                    return utc;
                }

                /**
                 * Define el valor de la propiedad utc.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUtc(String value) {
                    this.utc = value;
                }

                /**
                 * Obtiene el valor de la propiedad local.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocal() {
                    return local;
                }

                /**
                 * Define el valor de la propiedad local.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocal(String value) {
                    this.local = value;
                }

            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="airportIATACode">
     *           &lt;complexType>
     *             &lt;simpleContent>
     *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *               &lt;/extension>
     *             &lt;/simpleContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="BlockOff">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ScheduleDateTime">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="RealDateTime" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="EstimatedDateTime" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="TakeOff" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="RealDateTime" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="EstimatedDateTime" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "airportIATACode",
        "blockOff",
        "takeOff"
    })
    public static class Departure {

        @XmlElement(required = true)
        protected LegType.Departure.AirportIATACode airportIATACode;
        @XmlElement(name = "BlockOff", required = true)
        protected LegType.Departure.BlockOff blockOff;
        @XmlElement(name = "TakeOff")
        protected LegType.Departure.TakeOff takeOff;

        /**
         * Obtiene el valor de la propiedad airportIATACode.
         * 
         * @return
         *     possible object is
         *     {@link LegType.Departure.AirportIATACode }
         *     
         */
        public LegType.Departure.AirportIATACode getAirportIATACode() {
            return airportIATACode;
        }

        /**
         * Define el valor de la propiedad airportIATACode.
         * 
         * @param value
         *     allowed object is
         *     {@link LegType.Departure.AirportIATACode }
         *     
         */
        public void setAirportIATACode(LegType.Departure.AirportIATACode value) {
            this.airportIATACode = value;
        }

        /**
         * Obtiene el valor de la propiedad blockOff.
         * 
         * @return
         *     possible object is
         *     {@link LegType.Departure.BlockOff }
         *     
         */
        public LegType.Departure.BlockOff getBlockOff() {
            return blockOff;
        }

        /**
         * Define el valor de la propiedad blockOff.
         * 
         * @param value
         *     allowed object is
         *     {@link LegType.Departure.BlockOff }
         *     
         */
        public void setBlockOff(LegType.Departure.BlockOff value) {
            this.blockOff = value;
        }

        /**
         * Obtiene el valor de la propiedad takeOff.
         * 
         * @return
         *     possible object is
         *     {@link LegType.Departure.TakeOff }
         *     
         */
        public LegType.Departure.TakeOff getTakeOff() {
            return takeOff;
        }

        /**
         * Define el valor de la propiedad takeOff.
         * 
         * @param value
         *     allowed object is
         *     {@link LegType.Departure.TakeOff }
         *     
         */
        public void setTakeOff(LegType.Departure.TakeOff value) {
            this.takeOff = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;simpleContent>
         *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *     &lt;/extension>
         *   &lt;/simpleContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "value"
        })
        public static class AirportIATACode {

            @XmlValue
            protected String value;

            /**
             * Obtiene el valor de la propiedad value.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Define el valor de la propiedad value.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ScheduleDateTime">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="RealDateTime" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="EstimatedDateTime" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "scheduleDateTime",
            "realDateTime",
            "estimatedDateTime"
        })
        public static class BlockOff {

            @XmlElement(name = "ScheduleDateTime", required = true)
            protected LegType.Departure.BlockOff.ScheduleDateTime scheduleDateTime;
            @XmlElement(name = "RealDateTime")
            protected LegType.Departure.BlockOff.RealDateTime realDateTime;
            @XmlElement(name = "EstimatedDateTime")
            protected LegType.Departure.BlockOff.EstimatedDateTime estimatedDateTime;

            /**
             * Obtiene el valor de la propiedad scheduleDateTime.
             * 
             * @return
             *     possible object is
             *     {@link LegType.Departure.BlockOff.ScheduleDateTime }
             *     
             */
            public LegType.Departure.BlockOff.ScheduleDateTime getScheduleDateTime() {
                return scheduleDateTime;
            }

            /**
             * Define el valor de la propiedad scheduleDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link LegType.Departure.BlockOff.ScheduleDateTime }
             *     
             */
            public void setScheduleDateTime(LegType.Departure.BlockOff.ScheduleDateTime value) {
                this.scheduleDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad realDateTime.
             * 
             * @return
             *     possible object is
             *     {@link LegType.Departure.BlockOff.RealDateTime }
             *     
             */
            public LegType.Departure.BlockOff.RealDateTime getRealDateTime() {
                return realDateTime;
            }

            /**
             * Define el valor de la propiedad realDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link LegType.Departure.BlockOff.RealDateTime }
             *     
             */
            public void setRealDateTime(LegType.Departure.BlockOff.RealDateTime value) {
                this.realDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad estimatedDateTime.
             * 
             * @return
             *     possible object is
             *     {@link LegType.Departure.BlockOff.EstimatedDateTime }
             *     
             */
            public LegType.Departure.BlockOff.EstimatedDateTime getEstimatedDateTime() {
                return estimatedDateTime;
            }

            /**
             * Define el valor de la propiedad estimatedDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link LegType.Departure.BlockOff.EstimatedDateTime }
             *     
             */
            public void setEstimatedDateTime(LegType.Departure.BlockOff.EstimatedDateTime value) {
                this.estimatedDateTime = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "utc",
                "local"
            })
            public static class EstimatedDateTime {

                @XmlElement(required = true)
                protected String utc;
                protected String local;

                /**
                 * Obtiene el valor de la propiedad utc.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUtc() {
                    return utc;
                }

                /**
                 * Define el valor de la propiedad utc.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUtc(String value) {
                    this.utc = value;
                }

                /**
                 * Obtiene el valor de la propiedad local.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocal() {
                    return local;
                }

                /**
                 * Define el valor de la propiedad local.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocal(String value) {
                    this.local = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "utc",
                "local"
            })
            public static class RealDateTime {

                @XmlElement(required = true)
                protected String utc;
                protected String local;

                /**
                 * Obtiene el valor de la propiedad utc.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUtc() {
                    return utc;
                }

                /**
                 * Define el valor de la propiedad utc.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUtc(String value) {
                    this.utc = value;
                }

                /**
                 * Obtiene el valor de la propiedad local.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocal() {
                    return local;
                }

                /**
                 * Define el valor de la propiedad local.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocal(String value) {
                    this.local = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "utc",
                "local"
            })
            public static class ScheduleDateTime {

                @XmlElement(required = true)
                protected String utc;
                protected String local;

                /**
                 * Obtiene el valor de la propiedad utc.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUtc() {
                    return utc;
                }

                /**
                 * Define el valor de la propiedad utc.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUtc(String value) {
                    this.utc = value;
                }

                /**
                 * Obtiene el valor de la propiedad local.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocal() {
                    return local;
                }

                /**
                 * Define el valor de la propiedad local.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocal(String value) {
                    this.local = value;
                }

            }

        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="RealDateTime" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="EstimatedDateTime" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "realDateTime",
            "estimatedDateTime"
        })
        public static class TakeOff {

            @XmlElement(name = "RealDateTime")
            protected LegType.Departure.TakeOff.RealDateTime realDateTime;
            @XmlElement(name = "EstimatedDateTime")
            protected LegType.Departure.TakeOff.EstimatedDateTime estimatedDateTime;

            /**
             * Obtiene el valor de la propiedad realDateTime.
             * 
             * @return
             *     possible object is
             *     {@link LegType.Departure.TakeOff.RealDateTime }
             *     
             */
            public LegType.Departure.TakeOff.RealDateTime getRealDateTime() {
                return realDateTime;
            }

            /**
             * Define el valor de la propiedad realDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link LegType.Departure.TakeOff.RealDateTime }
             *     
             */
            public void setRealDateTime(LegType.Departure.TakeOff.RealDateTime value) {
                this.realDateTime = value;
            }

            /**
             * Obtiene el valor de la propiedad estimatedDateTime.
             * 
             * @return
             *     possible object is
             *     {@link LegType.Departure.TakeOff.EstimatedDateTime }
             *     
             */
            public LegType.Departure.TakeOff.EstimatedDateTime getEstimatedDateTime() {
                return estimatedDateTime;
            }

            /**
             * Define el valor de la propiedad estimatedDateTime.
             * 
             * @param value
             *     allowed object is
             *     {@link LegType.Departure.TakeOff.EstimatedDateTime }
             *     
             */
            public void setEstimatedDateTime(LegType.Departure.TakeOff.EstimatedDateTime value) {
                this.estimatedDateTime = value;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "utc",
                "local"
            })
            public static class EstimatedDateTime {

                @XmlElement(required = true)
                protected String utc;
                protected String local;

                /**
                 * Obtiene el valor de la propiedad utc.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUtc() {
                    return utc;
                }

                /**
                 * Define el valor de la propiedad utc.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUtc(String value) {
                    this.utc = value;
                }

                /**
                 * Obtiene el valor de la propiedad local.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocal() {
                    return local;
                }

                /**
                 * Define el valor de la propiedad local.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocal(String value) {
                    this.local = value;
                }

            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="utc" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="local" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "utc",
                "local"
            })
            public static class RealDateTime {

                @XmlElement(required = true)
                protected String utc;
                protected String local;

                /**
                 * Obtiene el valor de la propiedad utc.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUtc() {
                    return utc;
                }

                /**
                 * Define el valor de la propiedad utc.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUtc(String value) {
                    this.utc = value;
                }

                /**
                 * Obtiene el valor de la propiedad local.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocal() {
                    return local;
                }

                /**
                 * Define el valor de la propiedad local.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocal(String value) {
                    this.local = value;
                }

            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="routeTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "code",
        "description",
        "routeTypeCode"
    })
    public static class FlightServiceType {

        protected String code;
        protected String description;
        protected String routeTypeCode;

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

        /**
         * Obtiene el valor de la propiedad description.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Define el valor de la propiedad description.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

        /**
         * Obtiene el valor de la propiedad routeTypeCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRouteTypeCode() {
            return routeTypeCode;
        }

        /**
         * Define el valor de la propiedad routeTypeCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRouteTypeCode(String value) {
            this.routeTypeCode = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="legStatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="itineraryStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="operationalStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="suffixCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "legStatusCode",
        "itineraryStatusCode",
        "operationalStatusCode",
        "suffixCode"
    })
    public static class OperationalInformation {

        @XmlElement(required = true)
        protected String legStatusCode;
        protected String itineraryStatusCode;
        protected String operationalStatusCode;
        protected String suffixCode;

        /**
         * Obtiene el valor de la propiedad legStatusCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLegStatusCode() {
            return legStatusCode;
        }

        /**
         * Define el valor de la propiedad legStatusCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLegStatusCode(String value) {
            this.legStatusCode = value;
        }

        /**
         * Obtiene el valor de la propiedad itineraryStatusCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getItineraryStatusCode() {
            return itineraryStatusCode;
        }

        /**
         * Define el valor de la propiedad itineraryStatusCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setItineraryStatusCode(String value) {
            this.itineraryStatusCode = value;
        }

        /**
         * Obtiene el valor de la propiedad operationalStatusCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOperationalStatusCode() {
            return operationalStatusCode;
        }

        /**
         * Define el valor de la propiedad operationalStatusCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOperationalStatusCode(String value) {
            this.operationalStatusCode = value;
        }

        /**
         * Obtiene el valor de la propiedad suffixCode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSuffixCode() {
            return suffixCode;
        }

        /**
         * Define el valor de la propiedad suffixCode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSuffixCode(String value) {
            this.suffixCode = value;
        }

    }

}
