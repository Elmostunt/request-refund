package com.latam.proref.operations.services;

import org.springframework.http.ResponseEntity;

import com.latam.proref.requestrefund.entities.RequestRefundRQ;
import com.latam.proref.requestrefund.entities.RequestRefundRS;

public interface RequestRefundService {

	public ResponseEntity<RequestRefundRS> process(RequestRefundRQ dataToRequest);

	public RequestRefundRQ createRequest();
	
	public RequestRefundRS ParserResponse();
}
