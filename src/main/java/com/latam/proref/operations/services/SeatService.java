package com.latam.proref.operations.services;

import com.latam.proref.seat.entities.SeatRQ;
import com.latam.proref.seat.entities.SeatRS;

public interface SeatService{
	//Procesado de Request de Seat, esto para traer data.
	public SeatRS process(SeatRQ dataToRequest);
	
	//Metodo para armar Request desde data proveniente de eventoMDB.
	public SeatRQ createRequest(String data);
	
	//Mapeo de Responses, se agregaria Data Relacionada a response de SEATRS
	public SeatRS parserResponse(SeatRS data);

}
