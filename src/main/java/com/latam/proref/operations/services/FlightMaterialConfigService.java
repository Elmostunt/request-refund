package com.latam.proref.operations.services;

import com.latam.proref.flightmaterial.entities.FlightMateriaConfigRQ;
import com.latam.proref.flightmaterial.entities.FlightMaterialConfigRS;

public interface FlightMaterialConfigService {

	public FlightMaterialConfigRS process(FlightMateriaConfigRQ dataToRequest);

	public FlightMateriaConfigRQ createRequest() ;
	
	public FlightMaterialConfigRS parserResponse();
}
