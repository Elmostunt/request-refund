package com.latam.proref.operations.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.latam.proref.flightmaterial.entities.FlightMateriaConfigRQ;
import com.latam.proref.flightmaterial.entities.FlightMaterialConfigRS;
import com.latam.proref.operations.services.FlightMaterialConfigService;

public class FlightMaterialConfigImpl implements FlightMaterialConfigService {

	// TODO Obtener Endpoint de servicio.
	static final String URL_SEAT = "http://localhost:8080/flightmaterialconfig";
	private final static Logger log = LoggerFactory.getLogger(FlightMaterialConfigImpl.class);

	public FlightMaterialConfigRS process(FlightMateriaConfigRQ dataToRequest) {

		RestTemplate restTemplateSeat = new RestTemplate();

		// TODO Logica para creacion de request.
		FlightMateriaConfigRQ request = createRequest();
		request = dataToRequest;

		// TODO Validar por Apikey?
		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.setContentType(MediaType.APPLICATION_JSON);

		// Data attached to the Entity request.
		HttpEntity<FlightMateriaConfigRQ> requestBody = new HttpEntity<>(request, headers);
		try {
			// Send request with POST method.
			ResponseEntity<FlightMaterialConfigRS> response = restTemplateSeat.postForEntity(URL_SEAT, requestBody,
					FlightMaterialConfigRS.class);
			if (response.getStatusCode().equals(HttpStatus.OK)) {
				log.info("FlightMaterialConfig Status Code->" + response.getStatusCodeValue());
				log.info("FlightMaterialConfig Config Number->" + response.getBody().getConfigNumber());
				log.info("FlightMaterialConfig Material Code->" + response.getBody().getMaterialCode());
				return response.getBody();
			} else {
				// TODO CREATE ERROR RESPONSE
				return new FlightMaterialConfigRS();
			}
		} catch (RestClientException e) {
			// TODO: handle exception
			log.error("Exception e->" + e.getMessage());
		}
		return new FlightMaterialConfigRS();
	}

	public FlightMateriaConfigRQ createRequest() {
		FlightMateriaConfigRQ request = new FlightMateriaConfigRQ();
		return request;
	}

	public FlightMaterialConfigRS parserResponse() {
		FlightMaterialConfigRS response = new FlightMaterialConfigRS();
		return response;
	}

}
