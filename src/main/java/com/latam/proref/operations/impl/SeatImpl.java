package com.latam.proref.operations.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.latam.proref.operations.services.SeatService;
import com.latam.proref.seat.entities.SeatRQ;
import com.latam.proref.seat.entities.SeatRS;

public class SeatImpl implements SeatService {
	
	static final String URL_SEAT = "http://localhost:8080/seat";
	private final static Logger log = LoggerFactory.getLogger(SeatImpl.class);

	@Override
	public SeatRS process(SeatRQ request) {
		// LLAMADA A OPERACION POR POST
		RestTemplate restTemplateSeat = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.setContentType(MediaType.APPLICATION_JSON);

		// Data attached to the request.
		HttpEntity<SeatRQ> requestBody = new HttpEntity<>(request, headers);
		try {
			// Send request with POST method.
			ResponseEntity<SeatRS> response = restTemplateSeat.postForEntity(URL_SEAT, requestBody, SeatRS.class);
			if (response.getStatusCode().equals(HttpStatus.OK)) {

				log.info("Asiento es ->"+ response.getBody().getSeatNumber());
				return response.getBody();
			} else {
				log.error("Something error!");
				SeatRS responseOut = parserResponse(response.getBody());
				return responseOut;
				//TODO Crear error response;
			}
		} catch (RestClientException e) {
			log.error("RestClientExeption ->"+ e.getMessage());
		}
		//TODO Return error response 
		return new SeatRS();
	}

	// TODO CREAR REQUESTS
	public SeatRQ createRequest(String data) {
		SeatRQ request = new SeatRQ();
		return request;
	}
	//TODO Map and return error response
	public SeatRS parserResponse(SeatRS data) {
		SeatRS response = new SeatRS();
		return response;
		
	}

}
