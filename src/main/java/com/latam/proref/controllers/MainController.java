package com.latam.proref.controllers;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

//import com.latam.proref.flightmaterial.entities.FlightMateriaConfigRQ;
//import com.latam.proref.flightmaterial.entities.FlightMaterialConfigRS;
//import com.latam.proref.operations.services.FlightMaterialConfigService;
//import com.latam.proref.operations.services.RequestRefundService;
//import com.latam.proref.operations.services.SeatService;
//import com.latam.proref.requestrefund.entities.RequestRefundRQ;
//import com.latam.proref.seat.entities.SeatRQ;
//import com.latam.proref.seat.entities.SeatRS;

/**
 * @author G.Carcamo Clase controladora principal para ejecucion de: 
 * Caso de uso 1 
 * Controlador propio: Evaluacion de devolucion 
 * Trigger : Evento de vuelo despegado.
 * Caso de uso 2
 * Controlador propio: Almacen de datos
 */

@ComponentScan("com.latam")
@RestController
public class MainController {

//	private SeatService seatService;
//	
//	private FlightMaterialConfigService flightService;
//	
//	private RequestRefundService requestRefundService;

	//Caso de uso 1 -
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/cu1")
	public ResponseEntity<HttpStatus> process(@RequestBody String process) {
		if (process.equals("true")) {

			// 1.-Call to SEAT------------------------------------------------------//
//			SeatRQ requestSeat = new SeatRQ();
//			requestSeat = seatService.createRequest("OBJETO_A_MAPEAR");
//			SeatRS responseSeat=seatService.process(requestSeat);

			// 2.-Call to FlightMaterialConfig--------------------------------------//
//			FlightMateriaConfigRQ flightRQ = new FlightMateriaConfigRQ();
//			flightRQ = flightService.createRequest();
//			FlightMaterialConfigRS flightResponse = flightService.process(flightRQ);

			// 3.-Conexion with DAL

			// 4.-RequestRefund call
//			RequestRefundRQ dataToRequest = new RequestRefundRQ();
//			dataToRequest = requestRefundService.createRequest();			
//			requestRefundService.process(dataToRequest);

			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/cu2")
	public ResponseEntity<String> processCu2(@RequestBody String initiator) {
		if (initiator.equals("true")) {
			
			// 1.-Call to SEAT------------------------------------------------------//
//			SeatRQ requestSeat = new SeatRQ();
//			requestSeat = seatService.createRequest("OBJETO_A_MAPEAR");
//			SeatRS responseSeat=seatService.process(requestSeat);

			// 2.-Call to FlightMaterialConfig--------------------------------------//
//			FlightMateriaConfigRQ flightRQ = new FlightMateriaConfigRQ();
//			flightRQ = flightService.createRequest();
//			FlightMaterialConfigRS flightResponse = flightService.process(flightRQ);

			// 3.-Conexion with DAL
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}
}
