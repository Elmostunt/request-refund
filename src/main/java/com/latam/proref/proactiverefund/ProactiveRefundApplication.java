package com.latam.proref.proactiverefund;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

//CU001	Procesar compras de asientos
//CU002	Generar listado de devoluciones

//TODO Creado Util para conversion json a object
@ComponentScan("com.latam")
@SpringBootApplication
public class ProactiveRefundApplication {

	public static void main(String[] args) {

		SpringApplication.run(ProactiveRefundApplication.class, args);

	}
}
